from collections import Counter

# https://de.wikipedia.org/wiki/Buchstabenh%C3%A4ufigkeit
# Original list:  ENISRATDHULCGMOBWFKZPVJYXQ
substitutions = " ENRSCHUAILDVKOMTFBGZPWJYXQ"
corrected = " EINRSZUBCHTLVGAFDMOK" # fill this list with found substitutions, manually as decoding
encoded = "RTI TIWTN FTNMYGZHTMMTZHWL SVXXJ TM WIYGJ WHN PKNKHO KW FITZT MYGZHTMMTZ AH GKRTW TM PKNO KHYG WIYGJ XVTLZIYG MTIW PHNYG KWPTNT FTNOKGNTW PTW YVPT AH SWKYSTW"

char_counter = Counter(encoded) 
# sort from most to least occurance
char_counter = dict(sorted(char_counter.items(), key=lambda item: item[1], reverse=True)) 

occured_characters = char_counter.keys()

# generating decode table from the substitutions list
decode_table = {}
for letter, substitute in zip(occured_characters, substitutions[:len(occured_characters)]):
    print(f'{letter} -> {substitute}')
    decode_table[letter] = substitute

decoded = ''
for char in encoded:
    decoded += decode_table[char]

print(decoded, end='\n\n')

print("Corrected substitutions:")
for char in decoded:
    if char in corrected:
        print(char, end='')
    else:
        print("*", end='')
print()