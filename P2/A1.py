import random
random.seed()

def is_odd(num:int)->bool:
    return num % 2 != 0

# a^(n-1) mod n = +-1
# use pow(x, y z) to calculate x^y %z instead of brute force x**y, since pow(x,y,z) applies euler's theorem
def maybe_is_prime(n:int, loop_count:int)->bool:
    for i in range(loop_count):
        exponent = n - 1
        a = random.randint(2, n - 1)
        print(f"Randomly selected a = {a}")

        rest = pow(a, exponent, n)
        print(f"{a}^{exponent} % {n} = {rest}")
        if (rest != 1):
            return False # not primzahl

        exponent //= 2

        while True:
            rest = pow(a, exponent, n)
            print(f"{a}^{exponent} % {n} = {rest}")
            if (rest == 1):
                if (is_odd(exponent)):
                    break
                else:
                    exponent //= 2
            elif (rest == n - 1):
                break # done with this loop
            else:
                return False # not primzahl
        print()

    return True # wahrscheinlich primzahl

print()
print(f"\n{maybe_is_prime(10**100, 5)}")