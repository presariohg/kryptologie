import random
random.seed()

def is_odd(num:int)->bool:
    return num % 2 != 0

# a^(n-1) mod n = +-1
def maybe_is_prime(n:int, a:int)->bool:
    exponent = n - 1

    rest = pow(a, exponent, n)
    if (rest != 1):
        return False # not primzahl

    exponent //= 2

    while True:
        rest = pow(a, exponent, n)

        if (rest == 1):
            if (is_odd(exponent)):
                break
            else:
                exponent //= 2
        elif (rest == n - 1):
            break # done with this loop
        else:
            return False # not primzahl

    return True # wahrscheinlich primzahl

def count_non_prime_evidences(n:int)->int:

    evidence_count = 0
    for i in range(1, n):
        if (maybe_is_prime(n, i)):
            continue
        else:
            evidence_count += 1

    return evidence_count

n = 325
print(f"Zeugen count:\n{count_non_prime_evidences(n)}")