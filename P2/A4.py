import random
random.seed()

def is_odd(num:int)->bool:
    return num % 2 != 0

# a^(n-1) mod n = +-1
def maybe_is_prime(n:int, loop_count:int)->bool:
    for i in range(loop_count):
        exponent = n - 1
        a = random.randint(2, n - 1)

        rest = pow(a, exponent, n)
        if (rest != 1):
            return False # not primzahl

        exponent //= 2

        while True:
            rest = pow(a, exponent, n)
            if (rest == 1):
                if (is_odd(exponent)):
                    break
                else:
                    exponent //= 2
            elif (rest == n - 1):
                break # done with this loop
            else:
                return False # not primzahl

    return True # wahrscheinlich primzahl

def find_prime(n:int, loop_count:int)->int:
    if (is_odd(n)):
        pass
    else:
        n += 1

    while True:
        if (maybe_is_prime(n, loop_count)):
            return n
        else:
            n += 2

def avg_to_next_prime(upper_limit:int, draw_count:int, loop_count:int)->float:

    total_distance = 0
    for i in range(draw_count):
        n = random.randint(3, upper_limit)
        next_prime = find_prime(n, loop_count)
        # print(f"Drawn: {n}, next primzahl: {next_prime}")
        total_distance += next_prime - n

    return total_distance / draw_count

for exponent in range(1, 25):
    print(f"Limit 10^{exponent}, avg distance to next primzahl : {avg_to_next_prime(10**exponent, 1000, 20)}")
