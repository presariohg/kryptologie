import random
random.seed()

def is_odd(num:int)->bool:
    return num % 2 != 0

# a^(n-1) mod n = +-1
def maybe_is_prime(n:int, loop_count:int)->bool:
    for i in range(loop_count):
        exponent = n - 1
        a = random.randint(2, n - 1)

        rest = pow(a, exponent, n)
        if (rest != 1):
            return False # not primzahl

        exponent //= 2

        while True:
            rest = pow(a, exponent, n)
            if (rest == 1):
                if (is_odd(exponent)):
                    break
                else:
                    exponent //= 2
            elif (rest == n - 1):
                break # done with this loop
            else:
                return False # not primzahl

    return True # wahrscheinlich primzahl

def find_prime(n:int, loop_count:int)->int:

    if (is_odd(n)):
        pass
    else:
        n += 1

    while True:
        if (maybe_is_prime(n, loop_count)):
            return n
        else:
            n += 2

n = 10**100
print(f"Nearest primzahl >= {n}:\n{find_prime(n, 5)}")