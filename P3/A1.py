from helper import next_prime
import random
import math
from text2zahl import text2zahl, zahl2text
from key import n, e, d

foreign_n = 80838935643632347602689406251916492097256843104786631171171136456894311297084228755070725430240723776343069111225050752902571341583566366600578313441710584699499981601286903467954723125645821950643557
foreign_e = 23
# random.seed()

# p = next_prime(random.randrange(10**100))
# q = next_prime(random.randrange(10**100))

# n = p*q
# f = (p-1) * (q-1)
# while True:
#     e = random.randrange(1000)
#     if math.gcd(e, f) == 1:
#         break
#     else:
#         continue

# d = pow(e, -1, f)
# print(f"    p = {p}\n    q = {q}\n    n = {n}\n    f = {f}\n    e = {e}\n    \n    d = {d}")

# m = text2zahl('Wählen Sie einen sinnvollen Text (maximal 90 Zeichen)')
# m = text2zahl('Ja, das war ein sinnvoller Text.')
m = 51289299517868418068054052442069574670740736396295551378042330862951851017146
print(pow(m, foreign_e, foreign_n))

# c = 13926869316025095203193407104555881771181704232280264256041964782451749289446530311756899872041237790202509106955103842890107197618375382641470303096846548593144856446061181630578886871493688671199039

# print(pow(c, d, n))