from collections import Counter

encoded = open("code.txt").read().replace(" ", "")
char_counter = Counter(encoded)
char_counter = dict(sorted(char_counter.items(), key=lambda item: item[1], reverse=True)) 

## calculate char1 - char2 for key elimination
def char_sub(char1:chr, char2:chr)->chr:
    diff = ord(char1) - ord(char2)
    return chr(diff % 26 + ord('A'))

## calculate str1 - str2 for key elimination
def string_sub(str1:str, str2:str)->str:
    result = ''
    padding = 'A' * (len(str1) - len(str2))
    padded = str2 + padding

    for char1, char2 in zip(str1, padded):
        result += char_sub(char1, char2)

    return result


# Friedman test

# 1. Calculate Ko
N = len(encoded)
Ko = 0
for char, count in char_counter.items():
    Ko += (count * (count - 1)) / (N * (N - 1))

# 2. Estimate keylength
Kp = 2.05 / 26 # German Index of Coincidence
Kr = 1 / 26
keylength_estimated = (Kp - Kr) / (Ko - Kr) # = 11.5 

keylength = 9

divided = [""] * keylength
for index in range(len(encoded)):
    mod = index % keylength
    divided[mod] += encoded[index]

correcteds = ["EN",
              "EN",
              "EN",
              "EN",
              "EN",
              "EN",
              "EN",
              "EN",
              "EN",
              "EN"]
substitutions = ["ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ",
                 "ENISRATDHULCGMOBWFKZPVJYXQ"]

decodeds = [""] * keylength
for index in range(len(divided)):
    char_counter = Counter(divided[index]) 

    char_counter = dict(sorted(char_counter.items(), key=lambda item: item[1], reverse=True)) 
    occured_characters = char_counter.keys()

    # generating decode table from the substitutions list
    decode_table = {}
    for letter, substitute in zip(occured_characters, substitutions[index][:len(occured_characters)]):
        # print(f'{letter} -> {substitute}')
        decode_table[letter] = substitute

    decoded = ''
    for char in divided[index]:
        decoded += decode_table[char]

    # print(decoded, end='\n\n')

    # print("Corrected substitutions:")
    for char in decoded:
        if char in correcteds[index]:
            decodeds[index] += char
        else:
            decodeds[index] += "*"
    # print()

for i in range(len(decodeds[0])):
    for row in decodeds:
        try:
            print(row[i], end='')
        except IndexError:
            pass

print()
# import re

# sub_string_found = {}

# for index, char in enumerate(encoded):
#     # if index == len(encoded) - 1:
#     if index == len(encoded) - 2:
#         continue

#     # sub_string = char + encoded[index + 1]
#     try:
#         # sub_string = char + encoded[index + 1] + encoded[index + 2]
#         sub_string = char + encoded[index + 1] + encoded[index + 2] + encoded[index + 3] + encoded[index + 4]
#     except IndexError:
#         print (index)
#         break

#     if sub_string in sub_string_found.keys():
#         continue

#     occurences = list(re.finditer(sub_string, encoded))
#     if (len(occurences) > 1):
#         sub_string_found[sub_string] = [occurence.start() for occurence in occurences]
#         # sub_string_found[sub_string] = len(occurences)

# for key, value in sub_string_found.items():
#     print(f"{key} : {value}")
# print(keylength_estimated)

# might_contain = "SCHLUESSELN"
# might_contain = "VERSCHLUESSEL"
# might_contain = "ENTSCHLUESSEL"
# # might_contain = "KRYPTOLOGIE"
# might_contain = "DIEKRYPTOLOGIE"
# might_contain = "DENSCHLUESSEL"
# for keylength in range(4, 13):
#     if keylength > len(might_contain) - 3:
#         continue
#     strdiff = string_sub(encoded, encoded[keylength:])

#     guessdiff = string_sub(might_contain, might_contain[keylength:])
#     rep = guessdiff[:(len(might_contain)-keylength)]

#     print(f"{keylength}:{rep}")
#     if (rep in strdiff):
#         print(string_sub(encoded, might_contain))
#     else:
#         print("Failed")