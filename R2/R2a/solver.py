from collections import Counter

encoded = open("code.txt").read()
char_counter = Counter(encoded)
char_counter = dict(sorted(char_counter.items(), key=lambda item: item[1], reverse=True)) 

## calculate char1 - char2 for key elimination
def char_sub(char1:chr, char2:chr)->chr:
    diff = ord(char1) - ord(char2)
    return chr(diff % 26 + ord('A'))

## calculate str1 - str2 for key elimination
def string_sub(str1:str, str2:str)->str:
    result = ''
    padding = 'A' * (len(str1) - len(str2))
    padded = str2 + padding

    for char1, char2 in zip(str1, padded):
        result += char_sub(char1, char2)

    return result


# Friedman test

# 1. Calculate Ko
N = len(encoded)
Ko = 0
for char, count in char_counter.items():
    Ko += (count * (count - 1)) / (N * (N - 1))

# 2. Estimate keylength
Kp = 2.05 / 26 # German Index of Coincidence
Kr = 1 / 26
keylength_estimated = (Kp - Kr) / (Ko - Kr) # = 11.5 

# might_contain = "SCHLUESSELN"
# might_contain = "VERSCHLUESSEL"
# might_contain = "ENTSCHLUESSEL"
# might_contain = "KRYPTOLOGIE"
might_contain = "DIEKRYPTOLOGIE"
for keylength in range(4, 13):
    if keylength > len(might_contain) - 3:
        continue
    strdiff = string_sub(encoded, encoded[keylength:])

    guessdiff = string_sub(might_contain, might_contain[keylength:])
    rep = guessdiff[:(len(might_contain)-keylength)]
    print(f"{keylength}:{rep}")
    if (rep in strdiff):
        print(string_sub(encoded, might_contain))
    else:
        print("Failed")