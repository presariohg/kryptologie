from collections import Counter

## calculate char1 - or + char2
def char_calc(char1:chr, char2:chr, is_sub:bool)->chr:
    if is_sub:
        result = ord(char1) - ord(char2)
    else: # addition
        result = ord(char1) + ord(char2)

    return chr(result % 26 + ord('A'))

## calculate str1 - or + str2
def string_calc(str1:str, str2:str, is_sub:bool)->str:
    result = ''
    padding = 'A' * (len(str1) - len(str2))
    padded = str2 + padding

    for char1, char2 in zip(str1, padded):
        result += char_calc(char1, char2, is_sub)

    return result

might_contain = "KRYPTOLOGIE"

code1 = "DVJTTGFEDZMBFQMDLRNNCVISFPZUUYUKRKBNOVFAIIPIUIPLZS"
code2 = "MVYNKVUXDBCOFYIASRPJTACAOUPUUCGMSXBURPHCTPZPPNIXJX"

c1_xor_c2 = string_calc(code1, code2, True)

for i in range(len(c1_xor_c2)-len(might_contain)):
    left_pad = 'A' * i
    padded = left_pad + might_contain
    # print(f"{' ' * i}v")
    
    result = string_calc(c1_xor_c2, padded, False)

    print(result[i : i + len(might_contain)])
    for j in range(len(result)):
        if (j >= i) and (j < i + len(might_contain)):
            print(result[j], end='')
        else:
            print('*', end='')

    print()