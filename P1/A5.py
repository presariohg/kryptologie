import pandas as pd
import numpy as np

pd.options.display.expand_frame_repr = False

def gcd_columns(a:int, b:int)->int:
    # bigger = max(a, b)
    # smaller = min(a, b)

    r = [a, b]
    # r = [bigger, smaller]
    q = [np.nan, np.nan]

    while (r[-2] % r[-1] != 0):
        # print(bigger)
        q.append(r[-2] // r[-1])
        r.append(r[-2] % r[-1])

    return r, q

# cx = d % m
def euclid_table(c, d, m)->pd.DataFrame:
    r, q = gcd_columns(c, m)
    # y = [0, 1]
    y = [1, 0]
    for i in range(2, len(q)):
        y.append(y[i - 2] - y[i - 1] * q[i])

    df = pd.DataFrame(data={'r' : r, 
                            'q' : q,
                            'y' : y})

    return df

def solve(c:int, d:int, m:int)->int:
    df = euclid_table(c, d, m)
    if verbose:
        print(df, end='\n\n')

    y = list(df['y'])
    r = list(df['r'])

    # no solution
    if (d % r[-1] != 0): 
        return -1

    return (d * y[-1] // r[-1]) % m

# c = [25, 86, 19, 6, 6, 9**100 + 1]
# d = [13, 13, 14, 3, 3, 8**100 + 1]
# m = [61, 61, 61, 15, 18, 10**100 + 1]
# x = []
c = [41]
d = [31]
m = [100]
x = []
# verbose = False
verbose = True

for i in range(len(c)):
    print(f"For {c[i]}x = {d[i]} mod {m[i]}, x = {solve(c[i], d[i], m[i])}")
