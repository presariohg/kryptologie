def gcd_step_counter(a:int, b:int)->int:
    bigger = max(a, b)
    smaller = min(a, b)

    counter = 1
    while (bigger % smaller != 0):
        # print(bigger)
        counter += 1
        temp = bigger % smaller
        bigger = smaller
        smaller = temp

    return counter

print(gcd_step_counter(9**100 + 1, 10**100 + 1))