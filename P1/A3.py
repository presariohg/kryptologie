import random

random.seed()

def gcd_step_counter(a:int, b:int)->(int, int):
    bigger = max(a, b)
    smaller = min(a, b)

    counter = 1
    while (bigger % smaller != 0):
        # print(bigger)
        counter += 1
        temp = bigger % smaller
        bigger = smaller
        smaller = temp

    return (counter, smaller)

def randomize_gcd(run_count:int, upper_limit:int)->float:

    total_steps = 0.
    for i in range(run_count):
        num1 = random.randint(1, upper_limit)
        num2 = random.randint(1, upper_limit)
        steps_count, gcd = gcd_step_counter(num1, num2)
        total_steps += steps_count

        print(f'gcd({num1}, {num2}) = {gcd} ({steps_count} steps)')

    return total_steps / run_count

run_count = 1000
# upper_limit = 500_000_000_000_000_000_000_000_000_000
upper_limit = 500_000_000_000
# n = 10^s, avg steps around 1,7~2s

avg_steps = randomize_gcd(run_count, upper_limit)
print(f"\n{run_count} runs for random numbers <{upper_limit}, average steps needed = {avg_steps:.2f}")