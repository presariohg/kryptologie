def gcd(a:int, b:int)->int:
    bigger = max(a, b)
    smaller = min(a, b)

    while (bigger % smaller != 0):
        # print(bigger)
        temp = bigger % smaller
        bigger = smaller
        smaller = temp

    return smaller

print(gcd(9**100 + 1, 10**100 + 1))
# print(gcd(8172, 12834))